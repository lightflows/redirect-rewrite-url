Installation
------------

* Add to all/modules or the modules directory of a specific site.
* Enable Redirect Rewrite URL.

The module will take effect immediately.


Recommended Modules
-------------------

* Pathologic performs the same effect on formatted text (it will alter URLs that point to a redirect).
